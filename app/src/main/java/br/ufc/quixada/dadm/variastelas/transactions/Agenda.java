package br.ufc.quixada.dadm.variastelas.transactions;

import java.util.Arrays;

public class Agenda {

    private String nome;
    private int idade;
    private Contato[] lista_elefone;

    public Agenda(String nome, Contato[] lista_telefone, int idade) {
        super();
        this.nome = nome;
        this.lista_elefone = lista_telefone;
        this.idade = idade;
    }

    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }
    public Contato[] getListaTelefone() {
        return lista_elefone;
    }

    public void setListaTelefone(Contato[] listaTelefone) {
        this.lista_elefone = listaTelefone;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    @Override
    public String toString() {
        return "Agenda [nome=" + nome + ", listaTelefone="
                + Arrays.toString(lista_elefone) + ", idade=" + idade + "]";
    }

}
