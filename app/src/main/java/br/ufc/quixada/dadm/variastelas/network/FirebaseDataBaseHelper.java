package br.ufc.quixada.dadm.variastelas.network;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import br.ufc.quixada.dadm.variastelas.transactions.Contato;

public class FirebaseDataBaseHelper {

    private FirebaseDatabase mDatabase;
    private DatabaseReference mRefereceContatos;

    private List<Contato> contatos = new ArrayList<>();

    public interface DataStatus {
        void DataIsLoaded(List<Contato> contatos, List<String> keys);
        void DataIsInserted();
        void DataIsUpdated();
        void DataIsDeleted();
    }

    public FirebaseDataBaseHelper(Integer idAgenda) {
        mDatabase = FirebaseDatabase.getInstance();
        mRefereceContatos = mDatabase.getReference("agenda/" + idAgenda + "/contatos");
    }

    public void readContatos(final DataStatus dataStatus) {
        mRefereceContatos.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                contatos.clear();

                List<String> keys = new ArrayList<>();

                for(DataSnapshot keyNode : dataSnapshot.getChildren()) {
                    keys.add(keyNode.getKey());
                    Contato contato = keyNode.getValue(Contato.class);
                    contato.setId(keyNode.getKey());
                    contatos.add(contato);
                }

                dataStatus.DataIsLoaded(contatos, keys);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void addContato(Contato contato, final DataStatus dataStatus) {
        String key = mRefereceContatos.push().getKey();
        mRefereceContatos.child(key).setValue(contato)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        dataStatus.DataIsInserted();
                    }
                });
    }

    public void editContato(String key, Contato contato, final  DataStatus dataStatus) {
        mRefereceContatos.child(key).setValue(contato)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        dataStatus.DataIsUpdated();
                    }
                });
    }

    public void deleteContato(String key, final DataStatus dataStatus) {
        mRefereceContatos.child(key).removeValue()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        dataStatus.DataIsDeleted();
                    }
                });
    }

}
