package br.ufc.quixada.dadm.variastelas;

import android.content.Intent;
import android.provider.SyncStateContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import java.util.List;

import br.ufc.quixada.dadm.variastelas.network.FirebaseDataBaseHelper;
import br.ufc.quixada.dadm.variastelas.transactions.Constants;
import br.ufc.quixada.dadm.variastelas.transactions.Contato;

public class ContactActivity extends AppCompatActivity {

    private EditText edtNome;
    private EditText edtTel;
    private EditText edtEnd;

    private boolean edit;
    private Integer agendaId;
    private String idContatoEditar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        edtNome = findViewById( R.id.editTextNome );
        edtTel = findViewById( R.id.editTextTel );
        edtEnd = findViewById( R.id.editTextEnd );

        edit = false;

        agendaId = (Integer) getIntent().getExtras().get( "agendaId" );

        if( getIntent().getExtras().get( "key" ) != null ){

            String nome = ( String) getIntent().getExtras().get( "nome" );
            String telefone = ( String) getIntent().getExtras().get( "telefone" );
            String endereco = ( String) getIntent().getExtras().get( "endereco" );
            idContatoEditar = (String) getIntent().getExtras().get( "key" );

            edtNome.setText( nome );
            edtTel.setText( telefone );
            edtEnd.setText( endereco );

            edit = true;
        }

    }

    public void cancelar( View view ){
        setResult( Constants.RESULT_CANCEL );
        finish();
    }

    public void adicionar( View view ) {
        String nome = edtNome.getText().toString();
        String telefone = edtTel.getText().toString();
        String endereco = edtEnd.getText().toString();

        Contato contato = new Contato(nome, telefone, endereco);

        if(edit) {
            new FirebaseDataBaseHelper(agendaId).editContato(idContatoEditar, contato, new FirebaseDataBaseHelper.DataStatus() {
                @Override
                public void DataIsLoaded(List<Contato> contatos, List<String> keys) {

                }

                @Override
                public void DataIsInserted() {

                }

                @Override
                public void DataIsUpdated() {
                    Intent intent = new Intent();
                    setResult(Constants.RESULT_EDIT, intent);
                    finish();
                }

                @Override
                public void DataIsDeleted() {

                }
            });
        } else {

            new FirebaseDataBaseHelper(agendaId).addContato(contato, new FirebaseDataBaseHelper.DataStatus() {
                @Override
                public void DataIsLoaded(List<Contato> contatos, List<String> keys) {

                }

                @Override
                public void DataIsInserted() {
                    Intent intent = new Intent();
                    setResult(Constants.RESULT_ADD, intent);
                    finish();
                }

                @Override
                public void DataIsUpdated() {

                }

                @Override
                public void DataIsDeleted() {

                }
            });
        }

    }
}
