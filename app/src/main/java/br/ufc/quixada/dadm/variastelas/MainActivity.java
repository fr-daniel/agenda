package br.ufc.quixada.dadm.variastelas;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.ufc.quixada.dadm.variastelas.network.FirebaseDataBaseHelper;
import br.ufc.quixada.dadm.variastelas.transactions.Constants;
import br.ufc.quixada.dadm.variastelas.transactions.Contato;

public class MainActivity extends AppCompatActivity {

    private int selected;
    private ArrayList<Contato> listaContatos;
    private ArrayAdapter adapter;
    private ListView listViewContatos;

    private Integer agendaId = 1;

    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        selected = -1;

        listaContatos = new ArrayList<Contato>();

        listViewContatos = ( ListView )findViewById( R.id.listViewContatos );
        adapter = new ArrayAdapter(MainActivity.this, android.R.layout.simple_list_item_1, listaContatos);
        listViewContatos.setAdapter( adapter );
        listViewContatos.setSelector( android.R.color.holo_blue_light );

        listViewContatos.setOnItemClickListener( new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
            {
                Toast.makeText(MainActivity.this, "" + listaContatos.get( position ).toString(), Toast.LENGTH_SHORT).show();
                selected = position;
            }
        } );

        progressBar = findViewById( R.id.progressBar );
        progressBar.setIndeterminate( true );
        progressBar.setVisibility( View.VISIBLE );

        updateListaContatos();

    }

    public void updateListaContatos(){
        new FirebaseDataBaseHelper(agendaId).readContatos(new FirebaseDataBaseHelper.DataStatus() {
            @Override
            public void DataIsLoaded(List<Contato> contatos, List<String> keys) {
                listaContatos.clear();
                listaContatos.addAll(contatos);
                adapter.notifyDataSetChanged();
                progressBar.setVisibility( View.INVISIBLE );
            }

            @Override
            public void DataIsInserted() {

            }

            @Override
            public void DataIsUpdated() {

            }

            @Override
            public void DataIsDeleted() {

            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate( R.menu.menu_main_activity, menu );
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected( MenuItem item ) {

        switch(item.getItemId())
        {
            case R.id.add:
                clicarAdicionar();
                break;
            case R.id.edit:
                clicarEditar();
                break;
            case R.id.delete:
                apagarItemLista();
                break;
            case R.id.settings:
                break;
            case R.id.about:
                break;
        }
        return true;
    }

    private void apagarItemLista(){
        if( listaContatos.size() > 0 && selected != -1 ){
            Contato contato = listaContatos.get(selected);
            String idContato = contato.getId();

            new FirebaseDataBaseHelper(agendaId).deleteContato(idContato, new FirebaseDataBaseHelper.DataStatus() {
                @Override
                public void DataIsLoaded(List<Contato> contatos, List<String> keys) {

                }

                @Override
                public void DataIsInserted() {

                }

                @Override
                public void DataIsUpdated() {

                }

                @Override
                public void DataIsDeleted() {
                    Toast.makeText( MainActivity.this,"Contato deletado!", Toast.LENGTH_SHORT).show();
                    updateListaContatos();
                }
            });
        } else {
            selected = -1;
        }

    }

    public void clicarAdicionar(){
        Intent intent = new Intent( this, ContactActivity.class );
        intent.putExtra( "agendaId", agendaId );
        startActivityForResult( intent, Constants.REQUEST_ADD );
    }

    public void clicarEditar(){

        if(selected != -1) {
            Intent intent = new Intent(this, ContactActivity.class);

            Contato contato = listaContatos.get(selected);

            intent.putExtra("agendaId", agendaId);

            intent.putExtra("key", contato.getId());
            intent.putExtra("nome", contato.getNome());
            intent.putExtra("telefone", contato.getTelefone());
            intent.putExtra("endereco", contato.getEndereco());

            startActivityForResult(intent, Constants.REQUEST_EDIT);
        }
    }


    @Override
    protected void onActivityResult( int requestCode, int resultCode, @Nullable Intent data ) {
        super.onActivityResult(requestCode, resultCode, data);

      if( requestCode == Constants.REQUEST_ADD && resultCode == Constants.RESULT_ADD ){
          Toast.makeText( this,"Contato adicionado", Toast.LENGTH_SHORT).show();
          updateListaContatos();
      } else if( requestCode == Constants.REQUEST_EDIT && resultCode == Constants.RESULT_ADD ){
          Toast.makeText( this,"Contato editado!", Toast.LENGTH_SHORT).show();
         updateListaContatos();
      }
      else if( resultCode == Constants.RESULT_CANCEL ){
            Toast.makeText( this,"Cancelado", Toast.LENGTH_SHORT).show();
      }
    }

}
